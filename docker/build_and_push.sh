#!/bin/sh
set -eu

if [ -z ${1+x} ]; then
  echo Missing name argument
  exit 1
fi
name=$1
basedir=$(dirname "$0")

docker build --tag "$CI_REGISTRY_IMAGE/$name:build_$CI_COMMIT_REF_SLUG" -f "$basedir/$name/Dockerfile" .
docker push "$CI_REGISTRY_IMAGE/$name:build_$CI_COMMIT_REF_SLUG"
