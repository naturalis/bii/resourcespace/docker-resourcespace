#!/bin/sh
set -eu

cd "$(dirname "$0")"

# check if VAULT_TOKEN is empty if so exit with error
if [ -z "$VAULT_TOKEN" ]; then
  echo "VAULT_TOKEN is empty - could not get token from vault"
  exit 1
fi
TEST_API_KEY=$(docker run --cap-add=IPC_LOCK --env VAULT_TOKEN hashicorp/vault:latest vault kv get -field TEST_API_KEY -address="$VAULT_SERVER_URL" "kv2/$VAULT_PATH/cicd")

image_version=build_$CI_COMMIT_REF_SLUG

mkdir -p "data"
chown 82:82 "data"

docker pull "$CI_REGISTRY_IMAGE/nginx:$image_version"
docker pull "$CI_REGISTRY_IMAGE/php-fpm:$image_version"
docker pull "registry.gitlab.com/naturalis/bii/linnaeus/docker-linnaeus/docker-proxy:latest"
docker pull "registry.gitlab.com/naturalis/bii/linnaeus/docker-linnaeus/mariadb:latest"
docker run --rm -v "$PWD":"$PWD":ro -v /var/run/docker.sock:/var/run/docker.sock:ro -e "IMAGE_VERSION=$image_version" \
  docker compose -f "$PWD/docker-compose.yml" up -d

sleep 15
docker run --rm -t -v "$PWD/postman":/etc/newman -v "$PWD/pictures":/tmp/pictures --network qa_web -e TEST postman/newman:5-alpine run smoke-test.json --insecure --env-var "admin_key=$TEST_API_KEY"
