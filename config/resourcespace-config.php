<?php
###############################
## ResourceSpace
## Local Configuration Script
###############################

# All custom settings should be entered in this file.
# Options may be copied from config.default.php and configured here.

# MySQL database settings
$mysql_server = getenv('MYSQL_HOST') ? : 'database';
$mysql_username = getenv('MYSQL_USER') ? : 'rs_user';
$mysql_password = getenv('MYSQL_PASSWORD') ? : 'rs_password';
$mysql_db =  getenv('MYSQL_DATABASE') ? : 'rs_password';

$mysql_bin_path = '/usr/bin';

# Base URL of the installation
$baseurl = getenv('BASE_URL') ? : 'https://resourcespace.naturalis.nl';

# Email settings
$email_from = getenv('ADMIN_EMAIL') ? : 'aut@naturalis.nl';
$email_notify = getenv('ADMIN_EMAIL') ? : 'aut@naturalis.nl';

$spider_password =  getenv('SPIDER_PASSWORD') ? : die('You need to set SPIDER_PASSWORD environment variable');
$scramble_key =  getenv('SCRAMBLE_KEY') ? : die('You need to set SCRAMBLE_KEY environment variable');
$api_scramble_key =  getenv('API_SCRAMBLE_KEY') ? : die('You need to set API_SCRAMBLE_KEY environment variable');

# Paths
$imagemagick_path = '/usr/bin';
$ghostscript_path = '/usr/bin';
$ffmpeg_path = '/usr/bin';
$exiftool_path = '/usr/bin';
$antiword_path = '/usr/bin';
$pdftotext_path = '/usr/bin';


#Design Changes
$slimheader=true;



/*

New Installation Defaults
-------------------------

The following configuration options are set for new installations only.
This provides a mechanism for enabling new features for new installations without affecting existing installations (as would occur with changes to config.default.php)

*/
                                
//$thumbs_display_fields = array(8,3);
$list_display_fields = array(8,3,12);
$sort_fields = array(12);

$homeanim_folder = 'filestore/system/slideshow';

// Set imagemagick default for new installs to expect the newer version with the sRGB bug fixed.
$imagemagick_colorspace = "sRGB";

$slideshow_big=true;
$home_slideshow_width=1400;
$home_slideshow_height=900;

$themes_simple_view=true;

$stemming=true;
$case_insensitive_username=true;
$user_pref_user_management_notifications=true;
$themes_show_background_image = true;

$use_zip_extension=true;
$collection_download=true;

$ffmpeg_preview_force = true;
$ffmpeg_preview_extension = 'mp4';
$ffmpeg_preview_options = '-f mp4 -b:v 1200k -b:a 64k -ac 1 -c:v h264 -c:a aac -strict -2';

$daterange_search = true;
$upload_then_edit = true;

$purge_temp_folder_age=90;
$filestore_evenspread=true;

$comments_resource_enable=true;

# To switch on debugging
#
#$debug_log=getenv('DEBUG_LOG') ? true : false;
#$debug_log_location=getenv('DEBUG_LOG') ? : '/var/log/rs_debug.log';



/* Linnaeus settings */
$view_title_field=51; 
$thumbs_display_fields=array(51,3);
$list_display_fields=array(51,3);
$enable_remote_apis=true;
